package top.hmtools.wxmp.message.reply.model;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 消息管理 -- 被动回复用户消息  
 * @author HyboWork
 *
 */
public enum EReplyMessages {

	/**
	 * 回复文本消息
	 */
	ReplyTextMessage("回复文本消息", ReplyTextMessage.class),
	
	/**
	 * 回复图片消息
	 */
	ReplyImageMessage("回复图片消息", ReplyImageMessage.class),
	
	/**
	 * 回复语音消息
	 */
	ReplyVoiceMessage("回复语音消息", ReplyVoiceMessage.class),
	
	/**
	 * 回复视频消息
	 */
	ReplyVideoMessage("回复视频消息", ReplyVideoMessage.class),
	
	/**
	 * 回复音乐消息
	 */
	ReplyMusicMessage("回复音乐消息", ReplyMusicMessage.class),
	
	/**
	 * 回复图文消息
	 */
	ReplyNewsMessage("回复图文消息", ReplyNewsMessage.class);

	private String eventName;

	private Class<? extends BaseMessage> className;

	private String remark;

	private EReplyMessages(String eName, Class<? extends BaseMessage> clazz) {
		this.eventName = eName;
		this.className = clazz;
	}

	public String getEventName() {
		return eventName;
	}

	public Class<?> getClassName() {
		return className;
	}

	public String getRemark() {
		return remark;
	}
	
	public BaseMessage getInstance(){
		try {
			return this.className.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}
