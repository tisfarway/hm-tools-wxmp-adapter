package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 消息类型枚举
 * 
 * @author Hybomyth
 *
 */
public enum TagMsgType {

	/**
	 * 文本消息
	 */
	text("text"),
	/**
	 * 图片消息
	 */
	image("image"),
	/**
	 * 音频消息
	 */
	voice("voice"),
	/**
	 * 视频消息
	 */
	mpvideo("mpvideo"),
	
	/**
	 * 音乐消息
	 */
	music("music"),
	
	/**
	 * 短（小）视频消息
	 */
	shortvideo("shortvideo"),
	/**
	 * 地理位置消息
	 */
	location("location"),
	/**
	 * 链接消息
	 */
	link("link"),
	/**
	 * 事件消息
	 */
	event("event"), 
	
	/**
	 * 卡券消息（注意图文消息的media_id需要通过上述方法来得到）
	 */
	wxcard("wxcard"),
	
	/**
	 * 图文消息
	 */
	mpnews("mpnews");

	/**
	 * 消息类型名称
	 */
	private String name;

	/**
	 * 构造函数
	 * 
	 * @param code
	 * @param name
	 */
	private TagMsgType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.valueOf(this.name);
	}
}
