package top.hmtools.httpclient.hm2020test;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpHost;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle;
import top.hmtools.wxmp.core.httpclient.HmHttpClientTools;
import top.hmtools.wxmp.core.httpclient.impl.ConnectionKeepAliveStrategyDemoImpl;
import top.hmtools.wxmp.core.httpclient.impl.HttpRequestRetryHandlerDemoImpl;

public class HmHttpClientToolsTest {
	private final Logger logger = LoggerFactory.getLogger(HmHttpClientToolsTest.class);
	private  ObjectMapper objectMapper;
	
	private String url = "http://news.baidu.com/widget?id=LocalNews&ajax=json&t=1587610748645";
	
	/**
	 * 1. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 */
	@Test
	public void fastTestAAA(){
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", null);
		System.out.println(result);
	}
	
	/**
	 * 2. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 * 带上一些请求参数
	 */
	@Test
	public void fastTestBBB(){
		Map<String,Object> params = new HashMap<>();
		params.put("aa", "aaaaaa");
		params.put("bb", 33);
		params.put("cc", 2.2);
		params.put("dd", true);
		
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", params);
		System.out.println(result);
	}
	
	/**
	 * 3. 基础快速测试示例
	 * http get 请求百度首页，获取 HTML 字符串并打印
	 * 增加一些配置
	 */
	@Test
	public void fastTestCCC(){
		//设置连接池的连接数
		HmHttpClientFactoryHandle.setPoolingHttpClientConnectionManager(1, 1);
		//设置长连接？？
		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(new ConnectionKeepAliveStrategyDemoImpl());
		//设置超时
		HmHttpClientFactoryHandle.setRequestConfigBuilder(1000, 1000, 1000);
		//设置重试方案
		HmHttpClientFactoryHandle.setHttpRequestRetryHandler(new HttpRequestRetryHandlerDemoImpl());
		//添加http代理（随便在网上找了个免费的代理）
		HmHttpClientFactoryHandle.addProxies(new HttpHost("14.20.235.73", 808,"http"));
		
		String result = HmHttpClientTools.httpGetReqParamRespString("https://www.baidu.com", null);
		System.out.println(result);
	}
	

	/**
	 * 测试 http get请求，服务端返回json格式字符串
	 */
	@Test
	public void testhttpGetReqParamRespJsonAAA(){
		Map<String,Object> params = new HashMap<>();
		params.put("aa", "aaaaaa");
		params.put("bb", 33);
		params.put("cc", 2.2);
		params.put("dd", true);
		for(int ii=0;ii<10;ii++){
			Object result = HmHttpClientTools.httpGetReqParamRespJson(url, params, Object.class);
			this.printFormatedJson(ii+"、反馈结果",result);
		}
	}
	
	@Test
	public void testhttpPostReqJsonRespString(){
		GetTokenParam params = new GetTokenParam();
		params.setApi_key("");
		params.setApi_secret("");
		params.setBiz_no(String.valueOf(System.currentTimeMillis()));
		params.setComparison_type("0");
//		params.setImage_ref1(new File("C:\\Users\\HyboWork\\Desktop\\tmp\\baiSe\\face images\\timg.jpg"));

		params.setNotify_url("http://www.baidu.com");
		params.setReturn_url("http://www.baidu.com");
		params.setUuid(UUID.randomUUID().toString());
		
		String result = HmHttpClientTools.httpPostReqJsonRespString("https://api.megvii.com/faceid/lite/get_token", params,null);
		this.printFormatedJson("反馈结果",result);
	}
	
	/**
	 * 测试文件上传，post form提交
	 */
	@Test
	public void testhttpPostReqFormRespStringAAA(){
		GetTokenParam params = new GetTokenParam();
		params.setApi_key("");
		params.setApi_secret("");
		params.setBiz_no(String.valueOf(System.currentTimeMillis()));
		params.setComparison_type("0");
//		params.setImage_ref1(new File("C:\\Users\\HyboWork\\Desktop\\tmp\\baiSe\\face images\\timg.jpg"));

		params.setNotify_url("http://www.baidu.com");
		params.setReturn_url("http://www.baidu.com");
		params.setUuid(UUID.randomUUID().toString());
		
		String result = HmHttpClientTools.httpPostReqFormRespString("https://api.megvii.com/faceid/lite/get_token", params);
		this.printFormatedJson("反馈结果",result);
	}
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
}
