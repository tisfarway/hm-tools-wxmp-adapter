# 指南
## 使用场景
Java web项目开发过程中，一个html页面中必然会需要引用JavaScript文件及css文件，以JavaScript文件为例，比如要分别引用 a.js,b.js。  
一般情况下，会使用2个`“<script>”`标签引用，即：
```
<script src="a.js"></script>
<script src="b.js"></script>
```
这样会存在浏览器向服务器发送2次http请求，才能获取完所需要的JavaScript文件。
使用本工具后，则只需要一个标签就可以引入完2个JavaScript文件，即：
```
<script src="a.js,b.js"></script>
```
因而，在获取相同数据长度的情况下，通过减少了http请求次数，缩短了页面打开时间。

## 主要功能
- 根据URL请求参数将多个javascript文件合并成一个javascript文件返回给请求者。
- 根据URL请求参数将多个css文件合并成一个css文件返回给请求者。
- 支持从当前运行的spring boot工程jar包中读取javascript，css文件。
- 支持在请求URL携带字符编码名称获取相应的字符编码格式的js、css文件。
- 支持替换css文件中url引用资源的相对路径为可访问的绝对路径。
- 支持使用yuicompressor对js，css文件内容进行实时压缩、混淆。

## 特性
- 本项目基于spring boot starter，可独立运行（使用命令`java -jar spring-boot-starter-js-css-0.3.0.jar`）。也可通过maven pom.xml引入到您的spring boot项目中。可以完全零配置使用。
- 本项目能自动识别GBK、UTF-8字符编码，在读取磁盘JavaScript、css文件时，不需要手动统一指定何种字符编码，均可成功读取，不会乱码。在浏览器请求本项目获取JavaScript、css文件时，可以通过request param 指定任何合法的字符编码名称获取您所需要的字符编码的JavaScript、css文件内容。
- 本项目可以从jar包中读取JavaScript、css等静态资源文件，假设您基于spring boot开发项目，引入了本工具包，当你的项目以jar包形式发布运行时，您在该jar包中的所有JavaScript、css文件均可以读取出来，同时还支持列表展示。
- 本项目支持yuicompressor压缩、混淆JavaScript、css文件，您只需要访问本项目指定支持yuicompressor的接口，即可实现合并多个JavaScript文件并进行压缩、混淆。

## 快速开始
1. maven引用本项目jar包到您的基于spring boot项目：
```
<dependency>
    <groupId>top.hmtools</groupId>
    <artifactId>spring-boot-starter-js-css</artifactId>
    <version>0.3.0</version>
</dependency>
```
2. 启动您的spring boot 项目。
3. 假设您的spring boot项目监听的端口号是“8080”，在浏览器地址栏输入：“http://localhost:8080/list/js”,如果展示下图界面，说明OK了。
![](/js-files-list.jpg)

## 最新版本
[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/spring-boot-starter-js-css.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22spring-boot-starter-js-css%22)

<icp/>