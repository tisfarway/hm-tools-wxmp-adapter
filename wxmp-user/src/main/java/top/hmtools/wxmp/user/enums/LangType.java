package top.hmtools.wxmp.user.enums;

public enum LangType {

	/**
	 * 简体
	 */
	zh_CN ,
	/**
	 * 繁体
	 */
	zh_TW,
	/**
	 * 英语
	 */
	en ;
}
